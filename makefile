build:
	docker-compose build
up:
	docker-compose up
down:
	docker-compose down

bash\:backend:
	docker exec -it backend /bin/sh
bash\:database:
	docker exec -it database bash