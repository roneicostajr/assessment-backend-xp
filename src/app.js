const express = require('express');
const endpoints = require('./consts/endpoints');
const { categoryRoutes, productRoutes } = require('./routes');

require('./database');
require('dotenv').config();

const app = express();

app.use(express.json());
app.use(endpoints.category, categoryRoutes);
app.use(endpoints.product, productRoutes);

module.exports = app;
