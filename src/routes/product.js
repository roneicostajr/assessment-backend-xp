const router = require('express').Router();

const { productController } = require('../controllers');

router.post('', productController.post);
router.get('/:SKU', productController.getOne);
router.get('', productController.getAll);
router.put('/:SKU', productController.put);
router.delete('/:SKU', productController.delete);
module.exports = router;
