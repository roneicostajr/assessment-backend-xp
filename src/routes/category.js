const router = require('express').Router();

const { categoryController } = require('../controllers');

router.post('', categoryController.post);
router.get('', categoryController.getAll);
router.put('/:id', categoryController.put);
router.delete('/:id', categoryController.delete);

module.exports = router;
