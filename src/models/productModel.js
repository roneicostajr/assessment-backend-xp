const { default: mongoose } = require('mongoose');
const { productSchema } = require('../schemas/productSchema');

const product = mongoose.model('product', productSchema);

module.exports = {
  createProduct: async (productInfo) =>
    await product
      .create(productInfo)
      .then((res) => res)
      .catch((err) => {
        throw err;
      }),

  listAllProducts: async () =>
    await product
      .find({})
      .then((res) => res)
      .catch((err) => {
        throw err;
      }),

  getProductBySKU: async ({ SKU }) =>
    await product
      .findOne({ SKU })
      .exec()
      .then((res) => res)
      .catch((err) => {
        throw err;
      }),

  updateProduct: async ({ SKU }, productInfo) =>
    await product
      .findOneAndUpdate({ SKU }, productInfo, { runValidators: true })
      .exec()
      .then((docs) => docs)
      .catch((mongoError) => mongoError),

  deleteProduct: async (SKU) =>
    await product
      .findOneAndRemove({ SKU })
      .exec()
      .then((docs) => docs)
      .catch((mongoError) => mongoError),
};
