const categoryModel = require('./categoryModel');
const productModel = require('./productModel');

module.exports = { categoryModel, productModel };
