const { default: mongoose } = require('mongoose');
const { getDatabaseConnection } = require('../database');
const { categorySchema } = require('../schemas/categorySchema');

const db = getDatabaseConnection();

const category = mongoose.model('category', categorySchema);

module.exports = {
  createCategory: async ({ nome, codigo }) =>
    await category
      .create({ nome, codigo })
      .then((res) => res)
      .catch((err) => {
        throw err;
      }),

  getOneById: async (codigo) =>
    await category
      .findOne({ codigo })
      .exec()
      .then((res) => res)
      .catch((err) => {
        throw err;
      }),

  listAll: async () =>
    await category
      .find({})
      .exec()
      .then((docs) => docs)
      .catch((mongoError) => mongoError),

  updateCategory: async (codigo, nome) =>
    await category
      .findOneAndUpdate({ codigo }, { nome })
      .exec()
      .then((docs) => docs)
      .catch((mongoError) => mongoError),

  deleteCategory: async (codigo) =>
    await category
      .findOneAndRemove({ codigo })
      .exec()
      .then((docs) => docs)
      .catch((mongoError) => mongoError),
};
