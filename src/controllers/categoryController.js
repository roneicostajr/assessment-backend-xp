const { categoryModel } = require('../models');
const { formatResponse, formatMongoError } = require('../utils');

const categoryController = {
  post: async (req, res, next) => {
    await categoryModel
      .createCategory(req.body)
      .then(() =>
        res.json(formatResponse(null, 'Categoria criada com sucesso')),
      )
      .catch((err) => res.status(400).json(formatMongoError(err)));
  },

  getAll: async (req, res, next) => {
    await categoryModel
      .listAll()
      .then((categories) => {
        if (categories.length > 0)
          res.json(formatResponse(null, { categories }));
        else
          res.json(
            formatResponse({
              code: 'NO_RESULTS',
              msg: 'Não existem categorias cadastradas',
            }),
          );
      })
      .catch((err) => res.status(400).json(formatMongoError(err)));

    return next();
  },

  put: async (req, res, next) => {
    await categoryModel
      .updateCategory(req.params.id, req.body.nome)
      .then((category) => {
        if (category)
          res.json(formatResponse(null, 'Categoria atualizada com sucesso'));
        else
          res.json(
            formatResponse({
              code: 'NOT_FOUND',
              msg: 'Nao existe uma categoria com este codigo',
            }),
          );
      })
      .catch((err) => res.status(400).json(formatMongoError(err)));

    return next();
  },

  delete: async (req, res, next) => {
    await categoryModel
      .deleteCategory(req.params.id)
      .then((category) => {
        if (category)
          res.json(formatResponse(null, 'Categoria removida com sucesso'));
        else
          res.json(
            formatResponse({
              code: 'NOT_FOUND',
              msg: 'Nao existe uma categoria com este codigo',
            }),
          );
      })
      .catch((err) => res.status(400).json(formatMongoError(err)));

    return next();
  },
};

module.exports = categoryController;
