const { productModel } = require('../models');
const { formatMongoError, formatResponse } = require('../utils');

module.exports = {
  post: async (req, res, next) => {
    await productModel
      .createProduct(req.body)
      .then(() => res.json(formatResponse(null, 'Produto criado com sucesso')))
      .catch((err) => res.status(400).json(formatMongoError(err)));
    return next();
  },

  getOne: async (req, res, next) => {
    await productModel
      .getProductBySKU(req.params)
      .then((product) => {
        if (product) res.json(formatResponse(null, product));
        else
          res.json(
            formatResponse({
              code: 'NO_RESULTS',
              msg: 'Não existem produtos cadastrados com este SKU',
            }),
          );
      })
      .catch((err) => res.status(400).json(formatMongoError(err)));

    return next();
  },

  getAll: async (req, res, next) => {
    await productModel
      .listAllProducts()
      .then((products) => {
        if (products.length > 0) res.json(formatResponse(null, products));
        else
          res.json(
            formatResponse({
              code: 'NO_RESULTS',
              msg: 'Não existem produtos cadastrados',
            }),
          );
      })
      .catch((err) => res.status(400).json(formatMongoError(err)));

    return next();
  },

  put: async (req, res, next) => {
    await productModel
      .updateProduct(req.params, req.body)
      .then((product) => {
        if (!product.SKU) res.status(400).json(formatMongoError(product));
        else if (product)
          res.json(formatResponse(null, 'Produto atualizado com sucesso'));
        else
          res.json(
            formatResponse({
              code: 'NOT_FOUND',
              msg: 'Não existe um produto com este SKU',
            }),
          );
      })
      .catch((err) => res.status(400).json(formatMongoError(err)));

    return next();
  },

  delete: async (req, res, next) => {
    await productModel
      .deleteProduct(req.params.SKU)
      .then((product) => {
        if (product)
          res.json(formatResponse(null, 'Produto removido com sucesso'));
        else
          res.json(
            formatResponse({
              code: 'NOT_FOUND',
              msg: 'Não existe um produto com este SKU',
            }),
          );
      })
      .catch((err) => res.status(400).json(formatMongoError(err)));

    return next();
  },
};
