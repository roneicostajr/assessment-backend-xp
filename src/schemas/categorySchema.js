const { default: mongoose } = require('mongoose');

const { Schema } = mongoose;

module.exports.categorySchema = new Schema({
  nome: {
    type: String,
    required: [true, 'O campo "nome" é obrigatório'],
    unique: [true, 'Já existe uma categoria com este nome'],
  },
  codigo: {
    type: String,
    required: [true, 'O campo "código" é obrigatório'],
    unique: [true, 'Já existe uma categoria com este código'],
  },
});
