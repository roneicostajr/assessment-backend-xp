const { default: mongoose } = require('mongoose');

const { Schema } = mongoose;

module.exports.productSchema = new Schema({
  nome: {
    type: String,
    required: [true, 'O campo "nome" é obrigatório'],
    unique: true,
  },
  SKU: {
    type: String,
    required: [true, 'O campo "SKU" é obrigatório'],
    unique: true,
  },
  preco: {
    type: Number,
    required: [true, 'O campo "preco" é obrigatório'],
    min: [0, 'Um produto não pode ter um preço negativo'],
  },
  descricao: {
    type: String,
    required: [true, 'O campo "descricao" é obrigatório'],
  },
  quantidade: {
    type: Number,
    required: [true, 'O campo "quantidade" é obrigatório'],
    min: [0, 'A quantidade minima de um produto é 0'],
  },
  categorias: {
    type: Array,
    required: [true, 'O campo "categorias" é obrigatório'],
  },
});
