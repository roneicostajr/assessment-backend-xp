const request = require('supertest');
const endpoints = require('../../consts/endpoints');
const app = require('../../app');

describe('Produtos', () => {
  describe('POST', () => {
    it('Deve criar um produto caso todos os campos estejam presentes', (done) => {
      request(app)
        .post(endpoints.product)
        .send({
          nome: 'Tenis Simples',
          SKU: '1265',
          preco: 325.92,
          descricao: 'Um tenis simples',
          quantidade: 10,
          categoria: ['Tenis', 'Promocao'],
        })
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).toMatchObject({
            err: null,
            msg: 'Produto criado com sucesso',
          });
          done();
        });
    });
    describe('Deve falhar com erro caso falte um campo', () => {
      it('Nome', (done) => {
        request(app)
          .post(endpoints.product)
          .send({
            //  Campo nome removido
            SKU: '1265',
            preco: 325.92,
            descricao: 'Um tenis simples',
            quantidade: 10,
            categoria: ['Tenis', 'Promocao'],
          })
          .then((res) => {
            expect(res.statusCode).toBe(400);
            expect(res.body).toMatchObject({
              err: {
                code: 'ERRO_DE_VALIDACAO',
              },
            });
            done();
          });
      });
      it('SKU', (done) => {
        request(app)
          .post(endpoints.product)
          .send({
            nome: 'Tenis Simples',
            preco: 325.92,
            descricao: 'Um tenis simples',
            quantidade: 10,
            categoria: ['Tenis', 'Promocao'],
          })
          .then((res) => {
            expect(res.statusCode).toBe(400);
            expect(res.body).toMatchObject({
              err: {
                code: 'ERRO_DE_VALIDACAO',
              },
            });
            done();
          });
      });
      it('Preco', (done) => {
        request(app)
          .post(endpoints.product)
          .send({
            nome: 'Tenis Simples',
            SKU: '1265',
            descricao: 'Um tenis simples',
            quantidade: 10,
            categoria: ['Tenis', 'Promocao'],
          })
          .then((res) => {
            expect(res.statusCode).toBe(400);
            expect(res.body).toMatchObject({
              err: {
                code: 'ERRO_DE_VALIDACAO',
              },
            });
            done();
          });
      });
      it('descricao', (done) => {
        request(app)
          .post(endpoints.product)
          .send({
            nome: 'Tenis Simples',
            SKU: '1265',
            preco: 325.92,
            quantidade: 10,
            categoria: ['Tenis', 'Promocao'],
          })
          .then((res) => {
            expect(res.statusCode).toBe(400);
            expect(res.body).toMatchObject({
              err: {
                code: 'ERRO_DE_VALIDACAO',
              },
            });
            done();
          });
      });
      it('Quantidade', (done) => {
        request(app)
          .post(endpoints.product)
          .send({
            nome: 'Tenis Simples',
            SKU: '1265',
            preco: 325.92,
            descricao: 'Um tenis simples',
            categoria: ['Tenis', 'Promocao'],
          })
          .then((res) => {
            expect(res.statusCode).toBe(400);
            expect(res.body).toMatchObject({
              err: {
                code: 'ERRO_DE_VALIDACAO',
              },
            });
            done();
          });
      });
    });

    it('Deve falhar com erro caso o ID esteja duplicado', (done) => {
      request(app)
        .post(endpoints.product)
        .then((res) => {
          expect(res.statusCode).toBe(400);
          expect(res.body).toMatchObject({
            err: {
              code: 'ERRO_DE_VALIDACAO',
            },
          });
          done();
        });
    });

    it('Deve falhar com erro caso a quantidade seja invalida', (done) => {
      request(app)
        .post(endpoints.product)
        .then((res) => {
          expect(res.statusCode).toBe(400);
          expect(res.body).toMatchObject({
            err: {
              code: 'ERRO_DE_VALIDACAO',
            },
          });
          done();
        });
    });
  });
  describe('GET', () => {
    it('Deve retornar todos os produtos cadastrados', (done) => {
      request(app)
        .get(endpoints.product)
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).toMatchObject({
            err: null,
          });
          done();
        });
    });
    it('Deve retornar um produto usando o ID', (done) => {
      request(app)
        .get(`${endpoints.product}/1265`)
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).toMatchObject({
            err: null,
          });
          done();
        });
    });
    it('Deve falhar com erro caso o id buscado não exista', (done) => {
      request(app)
        .get(`${endpoints.product}/1267`)
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).toMatchObject({
            err: {
              code: 'NO_RESULTS',
              msg: 'Não existem produtos cadastrados com este SKU',
            },
          });
          done();
        });
    });
  });
  describe('PUT', () => {
    it('Deve alterar um produto usando um ID válido', (done) => {
      request(app)
        .get(`${endpoints.product}/1265`)
        .send({
          nome: 'Tenis Simples',
          SKU: '1265',
          preco: 325.92,
          descricao: 'Um tenis simples',
          quantidade: 10,
          categoria: ['Tenis', 'Promocao'],
        })
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).toMatchObject({
            err: null,
          });
          done();
        });
    });
    it('Deve falhar com erro caso o ID seja inválido', (done) => {
      request(app)
        .put(`${endpoints.product}/1268`)
        .send({
          nome: 'Tenis Simples',
          preco: 325.92,
          descricao: 'Um tenis simples',
          quantidade: 10,
          categoria: ['Tenis', 'Promocao'],
        })
        .then((res) => {
          expect(res.statusCode).toBe(400);
          expect(res.body).toMatchObject({
            err: {
              code: 'DATABASE_ERROR',
            },
          });
          done();
        });
    });
    it('Deve falhar com erro caso os dados sejam inválidos', (done) => {
      request(app)
        .put(`${endpoints.product}/1265`)
        .send({
          nome: 'Tenis Simples',
          preco: -325.92,
          descricao: 'Um tenis simples',
          quantidade: 10,
          categoria: ['Tenis', 'Promocao'],
        })
        .then((res) => {
          expect(res.statusCode).toBe(400);
          expect(res.body).toMatchObject({
            err: { code: 'ERRO_DE_VALIDACAO' },
          });
          done();
        });
    });
  });

  describe('DELETE', () => {
    it('Deve excluir com sucesso um produto usando ID', (done) => {
      request(app)
        .delete(`${endpoints.product}/1265`)
        .then((res) => {
          expect(res.statusCode).toBe(200);
        });
      done();
    });
    it('Deve falhar com erro caso o ID seja inválido', (done) => {
      request(app)
        .delete(`${endpoints.product}/1265`)
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).toMatchObject({
            err: {
              code: 'NOT_FOUND',
              msg: 'Não existe um produto com este SKU',
            },
          });
          done();
        });
    });
  });
});
