const request = require('supertest');
const endpoints = require('../../consts/endpoints');
const app = require('../../app');
const { default: mongoose } = require('mongoose');
require('dotenv').config();

describe('Categorias', (done) => {
  // beforeAll(async () =>
  //   mongoose.connect(process.env.MONGO_TESTING_URI, {}, () => done()),
  // );

  describe('POST', () => {
    it('Deve criar uma categoria com sucesso', (done) => {
      request(app)
        .post(endpoints.category)
        .send({
          codigo: '078',
          nome: 'Tenis',
        })
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).toMatchObject({
            err: null,
          });
          done();
        })
        .catch((err) => {
          throw err;
        });
    });
    describe('Deve falhar com erro caso falte um campo', () => {
      it('Nome', (done) => {
        request(app)
          .post(endpoints.category)
          .send({
            codigo: '1265',
          })
          .then((res) => {
            expect(res.statusCode).toBe(400);
            expect(res.body).toMatchObject({
              err: {
                code: 'ERRO_DE_VALIDACAO',
                mongoError: ['nome'],
              },
            });
            done();
          })
          .catch((err) => {
            throw err;
          });
      });
      it('Codigo', (done) => {
        request(app)
          .post(endpoints.category)
          .send({
            nome: 'Categoria 1',
          })
          .then((res) => {
            expect(res.statusCode).toBe(400);
            expect(res.body).toMatchObject({
              err: {
                code: 'ERRO_DE_VALIDACAO',
                mongoError: ['codigo'],
              },
            });
            done();
          })
          .catch((err) => {
            throw err;
          });
      });
    });

    it('Deve falhar com erro caso o ID esteja duplicado', (done) => {
      request(app)
        .post(endpoints.category)
        .send({ nome: 'Categoria 1', codigo: '078' })
        .then((res) => {
          expect(res.statusCode).toBe(400);
          expect(res.body).toMatchObject({
            err: {
              code: 'DUPLICATED_FIELD',
            },
          });
          done();
        })
        .catch((err) => {
          throw err;
        });
    });
  });
  describe('GET', () => {
    it('Deve listar todas os categorias', (done) => {
      request(app)
        .get(endpoints.category)
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).toMatchObject({
            err: null,
          });
          done();
        })
        .catch((err) => {
          throw err;
        });
    });
  });

  describe('PUT', () => {
    it('Deve alterar um categoria usando um ID válido', (done) => {
      request(app)
        .put(`${endpoints.category}/078`)
        .send({
          nome: 'Codigo 2',
        })
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).toMatchObject({
            err: null,
          });
          done();
        })
        .catch((err) => {
          throw err;
        });
    });
    it('Deve falhar com erro caso o ID seja inválido', (done) => {
      request(app)
        .put(`${endpoints.category}/079`)
        .send({
          nome: 'Categoria 2',
        })
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).toMatchObject({
            err: {
              code: 'NOT_FOUND',
              msg: 'Nao existe uma categoria com este codigo',
            },
          });
          done();
        })
        .catch((err) => {
          throw err;
        });
    });
  });

  describe('DELETE', () => {
    it('Deve excluir com sucesso um categoria usando ID', (done) => {
      request(app)
        .delete(`${endpoints.category}/078`)
        .then((res) => {
          expect(res.statusCode).toBe(200);
          done();
        })
        .catch((err) => {
          throw err;
        });
    });
    it('Deve falhar com erro caso o ID seja inválido', (done) => {
      request(app)
        .delete(`${endpoints.category}/078`)
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).toMatchObject({
            err: {
              code: 'NOT_FOUND',
              msg: 'Nao existe uma categoria com este codigo',
            },
          });
          done();
        })
        .catch((err) => {
          throw err;
        });
    });
  });
});
