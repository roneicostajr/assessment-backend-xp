exports.formatResponse = (
  err = { code: 'ERROR', msg: 'Um erro desconhecido ocorreu' },
  msg = null,
) => {
  if (!msg) {
    return { err };
  }
  return { err, msg };
};

exports.formatMongoError = (mongoError) => {
  const { errors } = mongoError;
  if (errors) {
    const response = [];
    Object.keys(errors).forEach((error) => response.push(error));
    return { err: { code: 'ERRO_DE_VALIDACAO', mongoError: response } };
  }

  if (mongoError.code === 11000)
    return {
      err: {
        code: 'DUPLICATED_FIELD',
        mongoError: `Este ${
          Object.keys(mongoError.keyPattern)[0]
        } já está cadastrado`,
      },
    };

  return {
    err: {
      code: 'DATABASE_ERROR',
      mongoError: mongoError.message,
    },
  };
};
