const mongoose = require('mongoose');
require('dotenv').config();

mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true });

mongoose.connection.on(
  'error',
  console.error.bind(console, 'Connection Error'),
);

module.exports = {
  getDatabaseConnection() {
    return mongoose.connection;
  },
};
