FROM node:12.18.3-alpine as application-stage
WORKDIR /app
COPY package* /app/
RUN npm install
COPY . .