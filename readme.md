# Desafio WebJump

O desafio proposto foi resolvido utilizando Javascript e Node.js(v12.18). O servidor foi construido utilizando o pacote Express, os testes foram realizando utilizando Jest e o banco de dados escolhido foi o MongoDB.
Todo o projeto foi containerizado utilizando Docker(v20.10) e pode ser iniciado utilizando o docker-compose(v1.28)

# Como executar o projeto

1. Instale o
   [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) e
   [Docker Compose](https://docs.docker.com/compose/install) e siga as instruções em https://docs.docker.com/engine/install/linux-postinstall/

2. Execute o comando:

   ```
   make build
   ```

   o processo do build pode demorar alguns minutos, quando estiver completo execute,

   ```
   make up
   ```

# Considerações

- O servidor rodará na porta :8080<br />
- O arquivo projeto 'gojumper.postman_collection.json' pode ser importado no Postman.<br />
- As rotas utilizam padrão REST<br />
- Para realizar os testes unitários utilize:

  ```
  make bash:backend
  ```

  e

  ```
  npm run test
  ```
